#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	QPushButton *button = new QPushButton(ui->centralWidget);

	connect(button,
		SIGNAL(clicked()),
		this,
		SLOT(addNewLineEdit()));

	QVBoxLayout * layout = new QVBoxLayout;
	layout->addWidget(button);
	ui->centralWidget->setLayout(layout);

	button->setText("Add a new line edit");
}

void MainWindow::addNewLineEdit() 
{
	QLineEdit * newLineEdit = new QLineEdit();
	ui->centralWidget->layout()->addWidget(newLineEdit);
}

MainWindow::~MainWindow()
{
    delete ui;
}
